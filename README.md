# Pitch Deck Quick Start


A pitch deck (or slide deck) is a business slideshow presentation:

  * Summarizes the business idea and business model of a startup company. 
  * Often requested by investors prior to a company’s pitch presentation.
  * A carefully crafted pitch deck is a fundraising fundamental.


## Slide 1: Vision

* One sentence overview of your business.
* Emphasize the value that your business provides. 
* Keep it short and simple. 


## Slide 2: Problem

* Talk about the problem you are solving.
* Tell a relatable story when you are defining the problem. 
* The more you can make the problem as real as possible, the more your investors will understand your business and your goals.


## Slide 3: Target

* Talk about your ideal users.
* Who are they?
* What do they need?
* Where are they?
* When do they need your idea?
* How many are there?
* Why are they your target users?


## Slide 4: Solution

* Describe how customers use your product/service.
* How does it address the problem that you presented on slide 2?
* Use pictures and stories when you describe your solution. 


## Slide 5: Revenue

* What do you charge?
* Who pays the bills?
* Describe the competitive landscape.
* How does your pricing fit into the larger market?


## Slide 6: Validation

* Talk about any proof you have that validates your problem, target, solution, and revenue.
* What have you achieved, such as milestones or goals?
* What are your next steps to validate your plan?
* Investors want to see proof points because these reduce risk. 


## Slide 7: Marketing

* Talk about how you plan to get customers’ attention.
* Show you have a solid grasp of how you will reach your target market and what sales channels you plan on using.
* Investors know finding and winning customers can sometimes be the biggest challenge for a startup.
* If your sales and marketing is different than your competitors, then highlight the differences.


## Slide 8: Team

* Talk about your team.
* Why are these the right people to build this company?
* What experience do you have that others don’t? 
* Highlight the key team members, their previous accomplishments, and key expertise they bring to the table.
* If you don’t have a complete team yet, then identify the key positions that you still need to fill, and also why those positions are critical to company growth.


## Slide 9: Financials

* Show your sales forecast, profit and loss statement, and cash flow forecast for 3 years.
* Do not show in-depth spreadsheets because these are difficult to read in a presentation format. 
* Limit yourself to charts that show sales, total customers, total expenses, and profits.
* Prepare to discuss any underlying assumptions that you’ve made to arrive at your sales goals and what your key expense drivers are.
* Be realistic. 
* If you can explain your growth based on traction you already have or compared to similar company in a related industry, that is extremely useful.


## Slide 10: Competition

* Describe how you fit into the competitive landscape.
* Why will customers choose you?
* How are you different than the competitors and the alternatives?
* What key advantages do you have over the competition?
* Is there any "secret sauce" that you have and others don’t?


## Slide 11: Partnerships

* Talk about any strategic partnerships that are critical to success. 
* For example do you have any key intellectual property licensing partnership?
* Do you have any key distribution partnerships?
* How does your success rely on these types of partnerships?


## Slide 12: Investment

* Ask for the money. 
* Why do you need the amount of money you are requesting?
* What is your plan for using the money?


## SixArm.com top six questions

SixArm.com publishes the top six questions to answer immediately:

1. Vision: Can you write a simple sentence summary of your idea and its value?

2. Reason: What broad problem are you solving? What's a specific relatable example?

3. People: Who are you focusing on helping? How do you reach them?

4. Context: how does your idea fit among any related ideas, products, services, etc.?

5. Proof: how do you demonstrate that your idea is worthwhile to try?

6. Needs: What do you need to succeed, such as teammates, partnerships, investments, etc.?


## Twine alternative

The company Twine has shared its pitch deck slide goals. Twine has a supply side and a demand side.

1. Opportunity – What big market changes are happening? Why is now the time for your startup matters?
2. Problem - Demand-side – The problem companies have, our demand-side customer.
3. Problem - Supply-side – The problem creatives have, our supply-side customer.
4. Solution – How we solve this.
5. Solution – A bit more detail on our solution.
6. Stats – Supply-side – giving validation of why we matter.
7. Stats – Demand-side – our current focus.
8. Business model – Simply how we will monetize.
9. Route to market – High-level growth plans, more detail given in the appendix.
10. Team – Some validation on why we are the team that can pull this off.
11. Appendix – more specific details on financials, projections, competition and route to market.


## Links to more

* https://www.marsdd.com/mars-library/how-to-create-a-pitch-deck-for-investors/
* http://www.garage.com/files/PerfectingYourPitch.pdf
* https://www.twine.fm/blog/pitch-deck-used-to-raise-a-million/

## Thanks

* Wikipedia
* MarsDD
* Garage
* Shawn Carolan and Menlo Ventures
* Fred Wilson and AVC
* Stuart Logan and Twine
